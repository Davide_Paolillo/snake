import pygame
import time
import random
import math

class SnakeFood:
    def __init__(self, r, g, b, x_pos, y_pos, score):
        self.color = (r, g, b)
        self.position = (x_pos, y_pos)
        self.score = score

class SnakeGame:
    def __init__(self, width, height, speed = 10):
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))
        self.middle = [width / 2, height / 2, 10, 10]
        self.snakeColor = (255, 255, 255)
        self.screenColor = (0, 0, 0)
        self.gameOver = False
        self.sessionOver = False
        self.speed = speed
        pygame.display.set_caption("Squeaky Snake")

    def setSnakeColor(self, r, g, b):
        self.snakeColor = (r, g, b)

    def run(self):
        pygame.display.update()
        
        self.clock = pygame.time.Clock()
        
        self.x_position = self.middle[0]
        self.y_position = self.middle[1]
        
        self.x_movement = 0
        self.y_movement = 0
        
        self.food = None
        self.spawnedFood = 0
        
        self.snake = []
        self.snakeLength = 1
        self.score = 0
        
        while not self.gameOver:
            self._handleGameOver()
            
            while self.sessionOver:
                self.screen.fill((0, 0, 255))
                self._lostMessage("You Lost! Press C-Play Again or Q-Quit", (255, 255, 0))
                self._scoreText()
                pygame.display.update()
            
                for event in pygame.event.get():
                    self._bindQuitButton(event)
                    self._handleReplay(event)
            
            for event in pygame.event.get():
                self._bindQuitButton(event)
                self._handleMovement(event)
                       
            self.x_position += self.x_movement
            self.y_position += self.y_movement
            
            self.screen.fill(self.screenColor)
            
            self.drawSnake()
            self.drawFood()
            
            if abs(self.x_position - self.food.position[0]) <= 10 and abs(self.y_position - self.food.position[1]) <= 10:
                self.spawnedFood -= 1
                self.snakeLength += 1
                self.score += self.food.score
            
            self._scoreText()
            
            pygame.display.update()
            self.clock.tick(30)
        
        pygame.display.update()
        pygame.quit()
        quit()
    
    def drawSnake(self):
        head = [self.x_position, self.y_position]
        if head in self.snake and self.snakeLength > 1:
            self.sessionOver = True
            return
        else:
            self.snake.append(head)
        
        if len(self.snake) > self.snakeLength:
            self.snake.pop(0)
        
        for value in self.snake:
            pygame.draw.rect(self.screen, self.snakeColor, [value[0], value[1], 10, 10])
    
    def drawFood(self):
        if self.spawnedFood == 0:
            self.spawnedFood += 1
            self.food = self._generateFood()
        pygame.draw.rect(self.screen, self.food.color, [self.food.position[0], self.food.position[1], 10, 10])
    
    def _generateFood(self):
        return SnakeFood(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255), random.randint(10, self.width - 10), random.randint(10, self.height - 10), 10)
    
    def _bindQuitButton(self, event):
        if event.type == pygame.QUIT:
            self.sessionOver = True
            
    def _handleMovement(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                self.x_movement = -self.speed
                self.y_movement = 0
            elif event.key == pygame.K_RIGHT:
                self.x_movement = self.speed
                self.y_movement = 0
            elif event.key == pygame.K_UP:
                self.x_movement = 0
                self.y_movement = -self.speed
            elif event.key == pygame.K_DOWN:
                self.x_movement = 0
                self.y_movement = self.speed
                
    def _lostMessage(self, message, color):
        out = pygame.font.SysFont(None, 30).render(message, True, color)
        self.screen.blit(out, [0, self.height / 2])
    
    def _scoreText(self):
        out = pygame.font.SysFont(None, 20).render("Your score: " + str(self.score), True, (0, 255, 0))
        self.screen.blit(out, [0, 0])

    def _handleGameOver(self):
        if self.x_position >= self.width or self.x_position < 0 or self.y_position >= self.height or self.y_position < 0:
            self.sessionOver = True
    
    def _handleReplay(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                self.gameOver = True
                self.sessionOver = False
            if event.key == pygame.K_c:
                self.__init__(self.width, self.height, self.speed)
                self.run()

game = SnakeGame(400, 300, 4)

game.run()
        

